<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('v1/ussd','UssdController@index');
Route::post('payment','UssdController@payment');

/*
 * M-PESA payment validation url
 */
Route::post('c2b/v1/validate', function(Request $request){
    if($request->secret == env('VALIDATION_CODE')) {
       // Log::info($request->getContent());
        echo response()->json([
            'ResultCode' => 0,
            'ResultDesc' => 'Success',
            'ThirdPartyTransID' => 0
        ]);

        //insert transaction
    }
});
/*
 * M-PESA payment confirmation url
 */
Route::post('c2b/v1/confirm', function(Request $request){

    $log = false;
   // Log::info($request->getContent());
     if($request->secret == env('VALIDATION_CODE')) {

        $callbackJSONData=file_get_contents('php://input');
        if(!$callbackJSONData){
            Log::error('PAYMENT >> MPESA: No Body sent');
            return response('ERROR: NO REQUEST');
        }

        $callbackData=json_decode($callbackJSONData );
        if($log) {
            Log::info($request->getContent());
        }

        try {
            $resultCode = $callbackData->Body->stkCallback->ResultCode;
            $checkoutRequestID = $callbackData->Body->stkCallback->CheckoutRequestID;

            $sub = \App\Models\Checkouts::where('checkout_request_id', $checkoutRequestID)->first();


            if ($resultCode == '0') {
                $amount = $callbackData->Body->stkCallback->CallbackMetadata->Item[0]->Value;
                $receipt = $callbackData->Body->stkCallback->CallbackMetadata->Item[1]->Value;
                $trans_date = $callbackData->Body->stkCallback->CallbackMetadata->Item[2]->Value;
                $phone = $callbackData->Body->stkCallback->CallbackMetadata->Item[3]->Value;

                \App\Models\Checkouts::where('checkout_request_id', $checkoutRequestID)->update(['status' => 'Paid']);

                /*
                 * Insert the transaction details
                 */

                $data = ['transaction_reference' => $receipt,
                    'transaction_date' => date('Y-m-d', strtotime($trans_date)),
                    'transaction_time' => date('H:i:s', strtotime($trans_date)),
                    'sender_phone' => $phone,
                    'amount' => $amount,
                    'status' => 'New',
                ];

                \App\Models\Transactions::insert($data);
                //if ($sub->count() > 0) {
                    //$update['transaction_id'] = DB::getPdo()->lastInsertId();
               // }

            }
            echo response()->json([
                'ResultCode' => 0,
                'ResultDesc' => 'Confirmation received successfully'
            ],200);
        }
        catch (Exception $e){
            echo response()->json([
                'ResultCode' => $e->getCode(),
                'ResultDesc' => $e->getMessage()
            ], 201);
        }

    }
});
