<?php


namespace App\Loans;

class Techpitch {

	// api call
	private $business_code;
	private $timestamp;
	private $token;
	private $external_bulk_id;
	private $short_code;
	public $status;

	public function __construct($business_code, $username , $password , $shortcode)
	{
		$this->business_code = $business_code;
		$this->timestamp = date('YmdHis');
		$this->token = base64_encode(hash('sha256', $username.$password.$this->timestamp));
		$this->external_bulk_id = time();
		$this->short_code = $shortcode;

	}


	public function send_sms($recipient ,$message)
	{
		$this->api_url = "external-bulk/create";
		$this->api_parameters =
			["addresses" => [$recipient],
			 "business_code" => $this->business_code,
			 "external_bulk_id"=> rand(100000000000, 9999999999999999),
			 "token"=> $this->token,
			 "message" => $message,
			 "schedule_time" =>  date('Y-m-d H:i:s'),
			 "short_code"=> $this->short_code,
			 "timestamp"=> $this->timestamp,
			];

		$this->execute();
	}

	function execute()
	{

		$this->api_url = "http://196.13.121.195:9095/" . $this->api_url;
		$this->api_parameters = \GuzzleHttp\json_encode($this->api_parameters);
		//dd($this->api_url);

		// execute post
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->api_url);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($this->api_parameters))
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->api_parameters);
		curl_setopt($ch, CURLOPT_TIMEOUT, 180);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
		$results = curl_exec($ch);
		curl_close($ch);



		$response = json_decode($results, TRUE);
		//dd($response);
		$this->status = $response["responseDescription"];



	}

}

?>