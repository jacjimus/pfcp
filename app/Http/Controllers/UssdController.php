<?php
namespace App\Http\Controllers;

use App\Models\Donations;
use App\User;
use Mpesa;
use Response;
use App\models\Patients;
use App\Models\Customers;
use App\Models\Checkouts;
use Illuminate\Http\Request;
use App\Models\Customerbalances;
use App\Models\SMSNotification;
use App\Helpers\GeneralHelper;


class UssdController extends Controller
{

    public $msisdn;
    public $message;
    public $session_id;
    public $service_code;
    public $response;
    public $channel;
    public $customer;
    const SPLITTER_CHAR = '*';
    const CPY_NAME = "Pillar Foundation";
    const USSD_CODE = "*482*8#";
    const TILL_NO = "7117279";
    const ACC_TYPE = 'Bank';
    const ACC_NO = '';
    const PAYBILL = '7117279';
    const CUSTOMER_CARE = '0111857174';
    const STAFF_MAIN_MENU = "\n1.Register Field Officer\n 2.Register Donor\n3.Donate\n4.Register Patient\n 5.Invite a Friend\n0. Main menu";
    const FO_MAIN_MENU = "\n2.Register Donor\n3.Receive Donation\n4.Register Patient\n5.Invite a Friend\n0.Main menu";
    const DONOR_MAIN_MENU = "\n3.Donate\n4.Register Patient\n5.Invite a Friend\n0.Main menu";

    public function __construct() {
        $this->channel = '8';
    }

    public function index(Request $request)
    {
        $this->msisdn = $request->MSISDN;
        $this->message = $request->USSD_STRING;
        $this->session_id = $request->SESSION_ID;
        $this->service_code = $request->SERVICE_CODE;
//        \Log::channel('ussd')->warning(
//            'The request variables',
//            [
//                'context' => $request->USSD_STRING
//            ]
//    );
        $customer = Customers::find($this->formatPhoneNumber($this->msisdn));
        $this->customer = $customer;
        // Remove the USSD CODE and Channel
        if (substr( $this->message, 0, 2 ) === "8")
            $this->message = "";
        elseif (substr( $this->message, 0, 2 ) === "8*")
            $this->message = $this->str_replace_first($this->channel . '*', '', $this->message);
        //echo $this->message;die;
        /*
         * Split the text var by the *
         */
        // if (strpos($this->message, self::SPLITTER_CHAR) !== false) {
        $this->split_text = explode(self::SPLITTER_CHAR, $this->message);
        /*
         * After registration, Initialize the inputs replacing the registration parts
         */
        if (preg_match('/^[a-zA-Z]+\*[a-zA-Z]+\*[0-9]+\*[1-4]\*[1-4]\*?.+$/', $this->message)) {
            $this->message = preg_replace('/^[a-zA-Z]+\*[a-zA-Z]+\*[0-9]+\*[1-4]\*[1-4]\*/', '', $this->message);
        }
        /*
         * for every back | Home entry pick all the inputs after the 0 entry
         */
        if (preg_match('/^.+\*(0)\*?.+$/', $this->message)) {
            $this->message = preg_replace('/^.+\*(0)\*/', '', $this->message);
        }
        /*
         * Go back one step by removing last input from the user input
         */
        if (preg_match('/^(0)$/', end($this->split_text))) {
            // $this->message = str_replace( '*'. end($this->split_text) , '', $this->message);
            $this->message = '';
        }
        /*
         * Logout by enterring 00
         */
        if (preg_match('/^(00)$/', end($this->split_text))) {
            $this->response = "END Thank you for using our USSD service. Welcome back  by dialing " . self::USSD_CODE . ' ' . self::CPY_NAME;
        }
        if($this->message == ""):
            //Check if customer is registered
            if ($this->customer == null ):
                $this->response = "CON Welcome to " . self::CPY_NAME . " \n Enter your Fullname";
            elseif($this->customer <> null):
                if ($this->customer->is_staff())
                    $this->response = "CON Welcome  to " . self::CPY_NAME . self::STAFF_MAIN_MENU;
                elseif ($this->customer->is_field_officer())
                    $this->response = "CON Welcome to " . self::CPY_NAME . self::FO_MAIN_MENU;
                else
                    $this->response = "CON Welcome to " . self::CPY_NAME . self::DONOR_MAIN_MENU;
            endif;

        /*
        Suspend service by uncommenting the below line
        */
        //$this->response = "END Welcome to ". self::CPY_NAME." \n.We have suspended the service for 48hours . Call 0758852845 for assistance.";
        /*
         * User enters first name
         */
        elseif (preg_match('/^[a-z A-Z]+$/', $this->message)):

            if ($this->customer == null):
                $this->customer = $this->registerUser($this->message, $this->msisdn, 'Donor');
            endif;

            if($this->customer->is_staff()):
            $this->response = "CON Enter Field Officer ID number";
            elseif($this->customer->is_field_officer()):
                $this->response = "CON Welcome to ". self::CPY_NAME. self::FO_MAIN_MENU;
            else:
                $this->response = "CON Welcome to ". self::CPY_NAME. self::DONOR_MAIN_MENU;
            endif;
        elseif($this->message == '1'): // Registering a Field Officer
            if($this->customer <> null){
                $this->response = "CON You are not Authorized to Register Field Officers! Dial " . self::USSD_CODE . ' again to register';
            }
            elseif($this->customer->is_field_officer()){
                $this->response = "CON You are not Authorized to Register Field Officers! Dial! \n 0.Main Menu. \n 00.Logout ";
            }
            else{
                $this->response = "CON You are not Authorized to Register Field Officers! Dial! \n 0.Main Menu. \n 00.Logout ";
            }
        elseif($this->message == '2'): // Registering a Donor
            $this->response = 'CON Enter the full name of the donor';

        elseif(preg_match('/^(2)\*[a-z A-Z]+$/', $this->message)): // Registering a Donor
            $this->response = 'CON Enter mobile number of the donor';

        elseif(preg_match('/^(2)\*[a-z A-Z]+\*[0-9]+$/', $this->message)): // Registering a Donor
            $this->response = "CON Does the Donor want to be a partner?\n1. YES\n2. NO";

        elseif(preg_match('/^(2)\*[a-z A-Z]+\*[0-9]+\*[1-2]$/', $this->message)): // Registering a Donor

            $this->response = "CON Donor registered. To make a donation return to main menu and select option 3.\n 0. Main menu\n 00. Logout";

        elseif(preg_match('/^(3)\*[a-z A-Z]+$/' , $this->message) || $this->message == "3"): // Donate
            $this->response = "CON Enter the frequency to donate?\n1. Daily\n 2. Weekly\n3. Monthly\n4. One off";

        elseif(preg_match('/^(3)\*[1-4]$/', $this->message)):
            $this->response = "CON Enter the amount to donate\n00. Logout";

        elseif(preg_match('/^(3)\*[1-4]\*[0-9]+$/', $this->message)):
            $this->response = "CON Mode of Donation?\n1. M-PESA\n2.CASH\n 00. Logout";

        elseif(preg_match('/^(3)\*[1-4]\*[0-9]+\*[1-2]$/', $this->message)):
            $this->response = "CON Who is donating?\n1. Yourself\nOr Enter mobile of other donor\n 00. Logout";

        elseif(preg_match('/^(3)\*[1-4]\*[0-9]+\*[1-2]+\*[0-9]+$/', $this->message)):
            $frequency = ['1' => 'Daily' , '2'=>'Weekly', '3' => 'Monthly' , '4' => 'One-off'];
            $donation = Donations::firstOrNew(
                ['donor_id' => $this->customer->id]
            );
            $donation->frequency =  $frequency[$this->split_text[1]];
            $donation->amount = $this->split_text[2];
            $donation->initiator_id = $this->customer->id;
            $donation->save();
            if($this->split_text[3] == 1) {
                $phone = $this->split_text[4] == 1 ? $this->msisdn : $this->split_text[4];
                $this->stpPush($phone, $this->split_text[2]);
                $this->response = "END Check your Phone and Enter M-PESA pin to complete the transaction.";
            }
            else
            $this->response = "END Take Cash from the Donor. He/She should receive Donations Confirmation message shortly!";

        elseif($this->message == '4'): // Invite a friend to register
            $this->response = "CON Enter name of the Cancer Patient\n 00. Logout ";
        elseif(preg_match('/^(4)\*[a-z A-Z]+$/', $this->message)):
            $this->response = "CON Enter the mobile of the patient\n00. Logout";
        elseif(preg_match('/^(4)\*[a-z A-Z]+\*[0-9]+$/', $this->message)):
                Patients::firstOrCreate(['mobile' => $this->split_text[2]],
                    ['mobile' => $this->split_text[2],
                      'name' =>  $this->split_text[1],
                      'referral_id' => $this->customer->id]);
            $this->response = "CON Thank you for registering a cancer patient. We'll get back to him/her asap.\n 00. Logout";

        elseif(preg_match('/^(5)\*[0-9]+$/', $this->message)):
            $message = sprintf("Your friend %s recommended you to become a donor. Dial %s on your Safaricom Line.", $this->msisdn, self::USSD_CODE);
                $this->sendSms(end($this->split_text), $message);

            $this->response = "CON Your friend will get sms notification \n";

        endif;

        echo $this->response;

    }


    private function stpPush($phone , $amount)
    {
        Mpesa::c2bRegisterUrls();
        /** STK Push**/
        $simulateResponse = Mpesa::express($amount, $this->formatPhoneNumber($phone), '28290', 'Pillar Donation');

        $res = json_decode($simulateResponse);
        Checkouts::insert(['checkout_request_id' => $res->CheckoutRequestID, 'response' => $res->CustomerMessage, 'status' => 'Pending']);

    }

    private function formatPhoneNumber($no)
    {
        switch (true) {
            case (preg_match('#^7\d{8}$#', $no)):
                $no = '254' . $no;
                break;
            case (preg_match('#^1\d{8}$#', $no)):
                $no = '254' . $no;
                break;
            case (preg_match('#^07\d{8}$#', $no)):
                $no = '254' . substr($no, 1);
                break;
            case (preg_match('#^01\d{8}$#', $no)):
                $no = '254' . substr($no, 1);
                break;
            case (preg_match('#^2547\d{8}$#', $no)):
                break;
            case (preg_match('#^002547\d{8}$#', $no)):
                $no = substr($no, 2);
                break;
            case (preg_match('#^\+2547\d{8}$#', $no)):
                $no = substr($no, 1);
                break;
            default:
                throw new InvalidArgumentException('Invalid format supplied');
                break;
        }
        return $no;
    }
    private function str_replace_first($from, $to, $content)
    {
        $from = '/'.preg_quote($from , '/').'/';

        return preg_replace($from, $to, $content, 1);
    }

 private function registerUser($name , $phone, $user_type)
 {
     $customer = Customers::firstOrCreate( ['mobile' => $phone],
     ['name' =>  $name,
     'email' => $this->msisdn . '@gmail.com',
     'password' => bcrypt('secret'),
     'mobile' => $phone,
     'user_type' => $user_type]);
     return $customer;
 }

    private function sendSms($phone , $message){
        $sms = new SMSNotification;
        $sms->mobile = $phone;
        $sms->message = $message;
        $sms->status=0;
        $sms->response= 'waiting';
        $sms->save();
    }

    public function payment(Request $request)
    {
        Customerbalances::where("customer_mobile", $request->sender_phone)->increment('amount', $request->amount);
        $loan = Customerbalances::where("customer_mobile", $request->sender_phone)->first();
        if ($loan <> null):
            if ($loan->amount > $request->amount) {
                $message = sprintf("You have saved Ksh %s. You requested Loan of Ksh %s. Top up Ksh %s to ge the loan now!", GeneralHelper::formatMoney($request->amount), GeneralHelper::formatMoney($loan->amount), GeneralHelper::formatMoney((round($loan->amount / 5, 0) - $request->amount)));
                $this->sendSms($request->sender_phone, $message);
            }
            $message = sprintf("%s has deposited KES%s on the TILL No %s at %s", $request->first_name . " " . $request->last_name, GeneralHelper::formatMoney($request->amount), self::TILL_NO, GeneralHelper::formatDate($request->transaction_timestamp, "H:i P"));
            $this->sendSms("254722410268", $message);

        else:
            if($request->amount < 2000)
            $message = sprintf("%s ,you have registered with Ksh %s. Registration is Ksh 2,000. Top up to get the weekly amount of Ksh 10,000 starting now!", $request->first_name , GeneralHelper::formatMoney($request->amount));
            $this->sendSms($request->sender_phone, $message);

        endif;
    }
}




