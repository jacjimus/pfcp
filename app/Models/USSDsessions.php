<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class USSDsessions extends Model
{
    protected $table = "ussd_sessions";
    
    protected $primaryKey = 'session_id';
}
