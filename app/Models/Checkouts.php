<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkouts extends Model
{
    protected $fillable = ['created_at' , 'updated_at', 'checkout_request_id', 'status', 'response'];
}
