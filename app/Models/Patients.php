<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    protected $fillable = ['mobile' , 'name', 'referral_id'];
}
