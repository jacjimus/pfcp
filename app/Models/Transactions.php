<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    const STATUS_NEW = 'New';
    const STATUS_USED = 'Used';
    const STATUS_REVERSED = 'Reversed';

    protected $primaryKey = 'transaction_reference';

    public $incrementing = false;

    protected $fillable = ['transaction_reference','transaction_date', 'transaction_time','sender_phone','amount' ,'status', 'created_at' , 'updated_at' ];
}
