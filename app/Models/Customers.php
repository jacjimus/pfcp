<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $primaryKey = 'mobile';

    protected $fillable = ['name' , 'mobile' , 'email' , 'password' , 'user_type'];

    protected $table = 'users';

    public function is_staff()
    {
        return $this->user_type === 'Staff';
    }

    public function is_field_officer()
    {
        return $this->user_type === 'FO';
    }

    public function is_donor()
    {
        return $this->user_type === 'Donor';
    }

    public function donation()
    {
        return $this->hasOne(Donations::class, 'donor_id' , 'id');
    }

}
