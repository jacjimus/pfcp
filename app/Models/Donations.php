<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donations extends Model
{
    protected $fillable = ['donor_id' , 'amount', 'frequency', 'initiator_id'];
    public function donor()
    {
        return $this->belongsTo(Customers::class , 'donor_id' , 'id');
    }
}
