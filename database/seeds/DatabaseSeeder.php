<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'James Makau',
            'email' => 'jacjimus@gmail.com',
            'password' => bcrypt('secret'),
            'mobile' => '+254725830529,
            'user_type' => 'FO',
            'referred_by' => 0
        ]);
    }
}
