<?php

return [

    //Specify the environment mpesa is running, sandbox or production
    'mpesa_env' => 'production',
    /*-----------------------------------------
    |The App consumer key
    |------------------------------------------
    */
    'consumer_key'   => env('MPESA_KEY' , ''),

    /*-----------------------------------------
    |The App consumer Secret
    |------------------------------------------
    */
    'consumer_secret' => env('MPESA_SECRET' , ''),

    /*-----------------------------------------
    |The paybill number
    |------------------------------------------
    */
    'paybill'         => 4036751,

    /*-----------------------------------------
    |Lipa Na Mpesa Online Shortcode
    |------------------------------------------
    */
    'lipa_na_mpesa'  => env('PAYBILL' , ''),

    /*-----------------------------------------
    |Lipa Na Mpesa Online Passkey
    |------------------------------------------
    */
    'lipa_na_mpesa_passkey' => env('MPESA_PASS_KEY' , ''),

    /*-----------------------------------------
    |Initiator Username.
    |------------------------------------------
    */
    'initiator_username' => env('MPESA_INITIATOR' , ''),

    /*-----------------------------------------
    |Initiator Password
    |------------------------------------------
    */
    'initiator_password' => env('MPESA_INTITIATOR_PASS',''),

    /*-----------------------------------------
    |Test phone Number
    |------------------------------------------
    */
    'test_msisdn ' => '254722228313',

    /*-----------------------------------------
    |Lipa na Mpesa Online callback url
    |------------------------------------------
    */
    'lnmocallback' => 'https://loanco.bremwork.com/api/c2b/v1/confirm?secret=' . env('VALIDATION_CODE' , ''),

    /*-----------------------------------------
   |C2B  Validation url
   |------------------------------------------
   */
    'c2b_validate_callback' => 'https://loanco.bremwork.com/api/c2b/v1/validate?secret=' . env('VALIDATION_CODE' , ''),

    /*-----------------------------------------
    |C2B confirmation url
    |------------------------------------------
    */
    'c2b_confirm_callback' => 'https://loanco.bremwork.com/api/c2b/v1/confirm?secret=' . env('VALIDATION_CODE' , ''),

    /*-----------------------------------------
    |B2C timeout url
    |------------------------------------------
    */
    'b2c_timeout' => 'https://loanco.bremwork.com/api/c2b/v1/validate?secret=' . env('VALIDATION_CODE' , ''),

    /*-----------------------------------------
    |B2C results url
    |------------------------------------------
    */
    'b2c_result' => 'https://app.bremwork.com/api/c2b/v1/confirm?secret=' . env('VALIDATION_CODE' , ''),

];
